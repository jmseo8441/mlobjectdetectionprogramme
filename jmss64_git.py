########################
# Function Definitions #
########################

def YOLO(image_path, subfold = '', conf = 0.5, threshold = 0.3):
	""" Author: Jae Min Seo
		Uses YOLO (You Only Look Once) image detection to detect objects within a specified
		image. Uses pre-traned data from the COCO database.
		Parameters:
		-----------
		image_path: str
			A string containing the path to the image file
		subfold: str
			A string that states the name of the output folder to save the output files to
		conf: float
			A confidence threshold before bounding boxes are confirmed as a detection.
			Should be in the domain [0, 1]
		threshold: float
			A threshold value that
		Notes:
		------
		Uses the cv2_imshow(), not cv2.imshow() as an image displaying function. This hence should be
		changed accordingly if this is to be used outside of a Google Colab context.
		Label/Model/Configuration directories should also be changed accordingly if used outside of the
		Google Colab context.
	"""
	# Import modules
	import numpy as np
	import cv2
	from IPython.display import Image
	from glob import glob
	import os
	import random
	import time
	from google.colab.patches import cv2_imshow
	
	# Load the COCO class labels that the YOLO model was trained on
	labels = open("/mlobjectdetectionprogramme/coco.names").read().strip().split("\n")
	
	# Load YOLO object detector trained on COCO dataset with weights
	# and model configurations
	net = cv2.dnn.readNetFromDarknet("/mlobjectdetectionprogramme/yolov3.cfg", "/mlobjectdetection/yolov3.weights")
	
	# Initialise a list of colors to represent each possible class label
	np.random.seed(1)
	COLORS = np.random.randint(0, 255, size = (len(labels), 3),
		dtype = "uint8")

	# Grab associated spatial dimensions from image
	image = cv2.imread(image_path)
	(H, W) = image.shape[:2]

	# Determine only the output layer names that we need from YOLO
	ln = net.getLayerNames()
	ln = [ln[i[0] - 1] for i in net.getUnconnectedOutLayers()]

	# Construct a blob from the input image and then perform a forward
	# pass of the YOLO object detector, giving bounding boxes and probabilities
	blob = cv2.dnn.blobFromImage(image, 1 / 255.0, (416, 416),
		swapRB=True, crop=False)
	net.setInput(blob)
	start = time.time()
	Layers = net.forward(ln)
	end = time.time()
	tot_time = end - start

	# Display how long it took
	print("Detection took {:.6f} seconds".format(tot_time))

	# Initialise lists of detected bounding boxes, confidences and class IDs,
	# and central coordinates 
	boxes = []
	confidences = []
	classIDs = []
	central_coord = []

	# Loop over each of the layer outputs
	for output in Layers:
		# Loop over each of the detections
		for detection in output:
			# Extract the class ID and confidence (i.e., probability) of
			# the current object detection
			scores = detection[5:]
			classID = np.argmax(scores)
			confidence = scores[classID]

			# Only process if probability is greater than the minimum
			if confidence > conf:
				# Scale bounding box coordinates relative to image size
				box = detection[0:4]*np.array([W, H, W, H])
				(centerX, centerY, width, height) = box.astype("int")

				# Use center coordinates to derive top and left corner of 
				# bounding box
				x = int(centerX - (width / 2))
				y = int(centerY - (height / 2))
				central_coord.append([centerX, centerY])

				# Update list of bounding box coordinates, confidences,
				# and class IDs
				boxes.append([x, y, int(width), int(height)])
				confidences.append(float(confidence))
				classIDs.append(classID)

	# Non-maxima suppression to suppress weak, overlapping bounding boxes
	idxs = cv2.dnn.NMSBoxes(boxes, confidences, conf,threshold)

	# Create output file that summarises the data
	if subfold == '':
		txt_file = open(image_path.split('.')[0] +'_Output_Data.txt','w' )
	else:
		if not os.path.exists(subfold):
			os.makedirs(subfold)
		txt_file = open(subfold + os.sep + image_path.split('.')[0] +'_Output_Data.txt','w' )

	# Metadata
	txt_file.write('Objects detected for file '+ image_path + '\n')
	txt_file.write('Object, Confidence, Central Coordinates\n')

	# Ensure at least one detection exists
	if len(idxs) > 0:
		# Loop over the indices haven't eliminated
		for i in idxs.flatten():
			# Extract bounding box coordinates
			(x, y) = (boxes[i][0], boxes[i][1])
			(w, h) = (boxes[i][2], boxes[i][3])

			# Draw bounding box rectangle and label on image
			color = [int(c) for c in COLORS[classIDs[i]]]
			image = cv2.rectangle(image, (x, y), (x + w, y + h), color, 2)
			text = "{}: {:.1f}%".format(labels[classIDs[i]], confidences[i]*100)

			# Add data to .txt file
			txt_file.write("{}, {:.1f}%, {}\n".format(labels[classIDs[i]], confidences[i]*100,central_coord[i]))
			image = cv2.putText(image, text, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX,
				0.5, color, 2)
	else:
		print('No object detected for image: ' + image_path)
	txt_file.close

	# Save all new outputs into a subfolder if needed
	if subfold == '':
		cv2.imwrite('Processed_'+image_path, image)
		image = cv2.imread('Processed_'+ image_path)
	else:
		cv2.imwrite(subfold + os.sep + 'Processed_'+image_path, image)
		image = cv2.imread(subfold + os.sep + 'Processed_'+ image_path)
	
	# Display image
	cv2_imshow(image)

def ims2list():
	""" Author: Jae Min Seo
		Takes all image files in the working directory and concactenates their names into
		a list.
		Returns:
		--------
		imagee_names: iterable
			A list/iterable containing all of the images in the current/working directory.
		Notes:
		------
		Only collects images that are compatible with OpenCV.
	"""
	from glob import glob
	# Collect image files in list
	ims = ['.jpg', '.jpeg', '.jp2','.png', '.bmp', '.webp', '.pbm', '.pgm', '.ppm', 
		'.pxm', '.pnm', '.pfm', '.sr', '.ras', '.tiff', '.tif', '.exr', '.hdr', '.pic']
	image_names = []
	for im_type in ims:
		image_names.extend(glob('*' + im_type))
	return image_names
